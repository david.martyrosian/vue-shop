const mongoose = require('mongoose');
const userModel = require('./model/user');
mongoose.connect('mongodb://localhost:27017/vue-shop', {
    useNewUrlParser: true
}); // connect to our database

const db = mongoose.connection

db.on('error', console.error.bind(console, 'connection error:'))

module.exports = mongoose
