const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const bcrypt = require('bcrypt-nodejs');
const crypto = require('crypto');

const UserSchema = new Schema({
    firstName: String,
    lastName: String,
    fullName: String,
    phone: Number,
    email: String,
    isAdmin: Boolean,
    password: String,
    passwordToken: String,
    createdOn: String
});

UserSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

UserSchema.methods.generateRandomToken = function() {
    return crypto.randomBytes(16).toString('hex');
};

UserSchema.methods.verifyPassword = function(password, old) {
    return bcrypt.compareSync(password, this.password);
};

UserSchema.methods.comparePasswords = function(password, input) {
    return bcrypt.compareSync(password, input);
};

module.exports = mongoose.model('User', UserSchema)