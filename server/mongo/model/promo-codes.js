const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Promo = new Schema({
  code: String,
  activeFrom: Date,
  activeTo: Date,
  categories: Array,
  discount: Number
});

module.exports = mongoose.model('Promo', Promo);