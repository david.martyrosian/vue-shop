const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Order = new Schema({
	userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    cartProducts: Array,
    userDetails: {
    	city: String,
      phone: String,
      zip: String,
      fullName: String,
      comment: String,
      office: String,
      shippingType: String
    },
    status: {type: String, default: 'Обрабатывается'},
    totalPrice: Number,
    date: Date,
    promoCode: Object
});

module.exports = mongoose.model('Order', Order);