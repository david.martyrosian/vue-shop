const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ProductSchema = new Schema({
    productName: String,
    productDescription: String,
    productCategory: String,
    productImage: String,
    isTopProduct: Boolean,
    deleted: Boolean,
    files: Array,
    items: Array,
    date: Date,
    discount: Number
})

module.exports = mongoose.model('Product', ProductSchema)