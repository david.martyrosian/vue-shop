"use strict";
const fs = require('fs');
const mkdirp = require('mkdirp');

const forwarded = require('forwarded-host');

class Files {
  upload(req, res) {
    let file1 = req.files.file.path;
    let fileName = this._cleanName(req.files.file.originalFilename);
        
    let pid = '10000' + parseInt(Math.random() * 10000000) + `.${fileName}`;
    let folder = req.params.folder;
    this._saveLocal(file1, pid, folder, req, res);
  }

  
  _cleanName(name) {
    let fileExt = (/[.]/.exec(name)) ? /[^.]+$/.exec(name) : 'file';
    name = name.replace(fileExt, '').replace(/\s+/gi, '-').replace(/[^a-zA-Z0-9\-]/gi, '');
    return name += `.${fileExt}`;
  }


  _saveLocal(file, dest, folder, req, res) {
    if (!fs.existsSync(`public/files/${folder}`)){
      mkdirp.sync(`public/files/${folder}`);
    }

    if (typeof file === 'string') {
      let r = fs.createReadStream(file); 
      let w = fs.createWriteStream(`public/files/${folder}/${dest}`);

      r.pipe(w)
      .on('finish', (resp) => {
        if (!res.send) return res({'Location': `public/files/${folder}/${dest}`, 'key': dest});
        res.send(`${forwarded(req)}/public/files/${folder}/${dest}`);
      });
    }
  }
}

String.prototype.indexOfEnd = function(string) {
    var io = this.indexOf(string);
    return io == -1 ? -1 : io + string.length;
}

module.exports = new Files();