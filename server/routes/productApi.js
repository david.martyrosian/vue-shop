const express = require('express');
const router = express.Router();
const ProductModel = require('../mongo/model/product');
const PromoModel = require('../mongo/model/promo-codes');
const OrderModel = require('../mongo/model/order');
const UserModel = require('../mongo/model/user');


router.route('/products')
    .post((req, res) => {
        const product = new ProductModel(req.body);
        product.date = new Date();
        ////
        console.log(req.body);
        product.productName = req.body.product.name;
        product.files = [{'main': true, 'url': req.body.product.fileField}]
        product.items = [{'price': req.body.product.price}]
        console.log(product);

        ////
        product.save(product, (err, product) => {
          if (err) {
            res.send(err)
          } else {
            res.send(product)
          }
        });
    })
    .put((req, res) => {
      ProductModel.findOne({'_id': req.body._id}).then(product => {
        Object.keys(req.body).forEach(k => {
          if (k !== '_id' || k !== '__v') {
            product[k] = req.body[k];
          }
        });
        return product.save().then((product) => {
          res.send(product);
        }).catch(err => {
          res.sendStatus(500);
        })
      });
    })

    // Get All products
    .get((req, res) => {

        // UPDATE FUNCTION
        // ProductModel.find({}).then(products => {
        //   return products.reduce((pr, item) => pr.then(() => {
        //     return new Promise((resolve, reject) => {
        //       item.date = new Date();
        //       item.isTopProduct = true;
        //       console.log('Saving item');
        //       return item.save().then(resp => {
        //         setTimeout(function() {
        //           console.log('saved item');
        //           return resolve();
        //         }, 1000);
        //       })
        //     });
        //   }), Promise.resolve());
        // })
        // UPDATE FUNCTION

        let query =  {
          'deleted': {$ne: true}
        };

        if (req.query.ids) {
          query._id = {$in: req.query.ids.split(',')};
        }
        ProductModel.find(query).then(resp => {
            res.send(resp);
        }).catch(err => {
            res.status(500).send(err);
        })
    })

router.delete('/products/:id', function (req, res) {
  ProductModel.findOne({
    '_id': req.params.id
  }).then(item => {
    item.deleted = true;
    return item.save();
  }).then(() => {
    res.sendStatus(200);
  }).catch(err => {
    res.sendStatus(500);
  });
})

router.route('/products/top-product')
    .get(function (req, res) {
        ProductModel.find({
          'deleted': {$ne: true},
           'isTopProduct': true // disable temporarily to load all the products
        }, function (err, product) {
            if (err) {
              res.send(err);
            } else {
              res.json(product);
            }
        });
    });

router.route('/products/categories')
    .get(function (req, res) {
        ProductModel.find({'deleted': {$ne: true}}, {'productCategory': 1}).lean().then(products => {
          let categories = products.reduce((arr, pr) => {
            if (!arr.includes(pr.productCategory)) {
              arr.push(pr.productCategory);
            }

            return arr;
          }, []);

          res.send(categories);
        }).catch(err => {
          res.sendStatus(500);
        });
    });

router.route('/products/similar-product')
    .get(function (req, res) {
        ProductModel.find({
          'deleted': {$ne: true},
          productCategory: req.query['productCategory']
        }, function (err, product) {
            if (err) {
                res.send(err)
            }
            res.json(product)
        }).limit(4)
    })

router.route('/products/:product_id')
    .get(function (req, res) {
        ProductModel.findOne({'_id': req.params.product_id}).then(resp => {
            res.json(resp);
        }).catch(err => {
            res.send(err);
        });
    })

    .delete(function (req, res) {
        ProductModel.remove({
            _id: req.params.product_id
        },
            function (err, product) {
                if (err) {
                    res.send(err)
                }
                res.send(product)
            })
    })
router.put('/orders/:orderId/change-status', (req, res) => {
  if (!req.user.isAdmin) return res.sendStatus(403);

  return OrderModel.findByIdAndUpdate(req.params.orderId, {status: req.body.status}).then(() => {
    res.sendStatus(200);
  }).catch(err => {
    res.sendStatus(500);
  });
});

router.put('/orders/:orderId/cancel', (req, res) => {
  return OrderModel.findByIdAndUpdate(req.params.orderId, {status: 'Canceled'}).then(() => {
    res.sendStatus(200);
  }).catch(err => {
    res.sendStatus(500);
  });
});

router.route('/orders')
  .get((req, res) => {
    let query = req.user.isAdmin ? {} : {'userId': req.user._id};
    if (!req.user || !req.user._id) return res.sendStatus(401);
    return OrderModel.find(query).lean().then(orders => {
      if (!req.user.isAdmin) res.send(orders);
      let orderUsers = orders.map(o => o.userId.toString()).filter((u, ind, arr) => arr.indexOf(u) === ind);
      return UserModel.find({'_id': {$in: orderUsers}}).then(users => {
        orders = orders.map(order => {
          order.user = users.find(u => u._id.toString() === order.userId.toString());
          return order;
        });
        res.send(orders);
      });

    }).catch(err => {
      res.status(500).send(err);
    });
  })
  .post((req, res) => {
    let productIds = req.body.cartProducts.map(pr => pr.productId);
    let productsInfo = [];
    return ProductModel.find({'_id': {$in: productIds}})

    .then(products => {
      return req.body.cartProducts.reduce((pr, cartProduct) => {
        return pr.then(() => {
          
          let fullProduct = products.find(p => p._id.toString() === cartProduct.productId);

          if (!fullProduct) {
            return Promise.reject('No Product Found');
          } else {
            let productItem = fullProduct.items.find(i => i._id.toString() === cartProduct.selectedItem);
            if (!productItem) {
              return Promise.reject('No Product Item Found');
            } else if (Number(productItem.quantity) < 1) {
              return Promise.reject('No Product Item in Stock');
            } else {
              let productInfo = {
                productName: fullProduct.productName,
                productCategory: fullProduct.productCategory,
                productDescription: fullProduct.productDescription,
                files: fullProduct.files,
                itemType: productItem.type,
                itemPrice: productItem.price,
                discount: fullProduct.discount
              };
              productsInfo.push(productInfo);
              productItem.quantity = (Number(productItem.quantity) - 1).toString();
              fullProduct.markModified('items');
              return fullProduct.save();
            }
          }
        });
      }, Promise.resolve())
      .then(() => {
        let newOrder = new OrderModel({
          userId: req.user._id,
          cartProducts: productsInfo,
          userDetails: req.body.userDetails,
          totalPrice: req.body.totalPrice,
          promoCode: req.body.promoCode,
          date: new Date()
        });
        newOrder.save().then(order => {
          res.send(order);
        });
      });
    }).catch(err => {
      console.log('error here', err);
      res.status(500).send(err);
    });
  });

  router.get('/promo-codes/:code', (req, res) => {
    let query = {
      'code': req.params.code,
      activeTo: {"$gte": new Date()},
      activeFrom: {"$lt": new Date()}
    };
    return PromoModel.findOne(query).then((promo) => {
      if (!promo) return res.sendStatus(404);
      res.send(promo);
    }).catch(err => {
      res.status(500).send(err);
    });
  });

  router.get('/promo-codes', (req, res) => {
    return PromoModel.find().then((promos) => {
      res.send(promos);
    }).catch(err => {
      res.status(500).send(err);
    });
  });

  router.post('/promo-codes', (req, res) => {
    let newPromo = new PromoModel(req.body);
    return newPromo.save().then((promo) => {
      res.send(promo);
    }).catch(err => {
      res.status(500).send(err);
    });
  });

  router.put('/promo-codes', (req, res) => {
    return PromoModel.findOne({'_id': req.body._id}).then((promo) => {
      Object.keys(req.body).forEach(k => {
        if (k !== '_id' || k !== '__v') {
          promo[k] = req.body[k];
        }
      });
      return promo.save();
    })
    .then((promo) => {
      res.send(promo);
    })
    .catch(err => {
      res.status(500).send(err);
    });
  }); 

  router.delete('/promo-codes/:id', (req, res) => {
    return PromoModel.remove({'_id': req.params.id})
    .then((promo) => {
      res.sendStatus(200);
    }).catch(err => {
      res.status(500).send(err);
    });
  });


module.exports = router;








