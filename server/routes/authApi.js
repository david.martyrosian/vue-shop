const express = require('express');
const router = express.Router();
const passport = require('passport');
const nodemailer = require('nodemailer');
const isAuthenticated = require('../controller/auth.js');
const UserModel = require('../mongo/model/user');
const forwarded = require('forwarded-host');
const fs = require('fs');
const Handlebars = require('express-handlebars').create();

const transporter = nodemailer.createTransport({
 service: 'gmail',
 auth: {
  user: 'prozrachniy@gmail.com',
  pass: 'Transparent1221'
 }
});


router.post('/login', function(req, res, next) {
    passport.authenticate('local', { successRedirect: '/', failureRedirect: '/login' }, function(err, user, info) {
        if (err) {
            res.status(401).send(err);
        } else {
          req.logIn(user, loginErr => {
            if (loginErr) {
              return next(loginErr);
            }
            res.json(user);
          }); 
        }
    })(req, res, next)
});

router.get('/forgot-password/:token', function(req, res) {
  UserModel.findOne({'passwordToken': req.params.token}).then(user => {
    if (!user) return res.sendStatus(404);

    res.send(user._id);
  });
});

router.put('/forgot-password/:userId', function(req, res) {
  UserModel.findOne({'_id': req.params.userId}).then(user => {
    if (!user) return res.sendStatus(404);
    user.password = user.generateHash(req.body.password);
    user.passwordToken = undefined;
    user.save().then(user => {
      res.sendStatus(200);
    }).catch(err => {
      res.sendStatus(500);
    });
  });
});

router.post('/forgot-password', function(req, res) {
  console.log('this fires', req.body);
  UserModel.findOne({'email': req.body.email}).then(user => {

    if (!user) return res.sendStatus(404);

    user.passwordToken = user.generateRandomToken();
    user.save().then(user => {
      let defaultEmailTemplate = fs.readFileSync(process.cwd() + '/server/templates/default-template.html', 'utf8');
      let mailTemplate = Handlebars._compileTemplate(defaultEmailTemplate);
      let mailContent = mailTemplate({content: `Здравствуйте, ${user.fullName}. <br> Мы получили запрос на смену пароля авторизации. <br> Для завершения сброса пароля и установки нового, пожалуйста, кликните <a href="${forwarded(req) + '/forgot-password/' + user.passwordToken}">здесь.</a> <br><br> Если вы не запрашивали смену пароля, пожалуйста, свяжитесь с нами по номеру +380000000 или по email  <font color="blue"><a href="mailto:support@mail.com" target="_blank">support</a></font> `});

      const mailOptions = {
        from: 'Infamiss Shop <prozrachniy@gmail.com>',
        to: user.email,
        subject: 'Восстановление Пароля',
        html: mailContent
      };

      transporter.sendMail(mailOptions, function (err, info) {
         if (err) {
           res.sendStatus(500);
         } else {
           res.sendStatus(200);
         }
      });
    });
  }).catch(err => {
    console.log(err);
  })
});

router.get('/logout', (req, res) => {
  req.logout();
  res.sendStatus(200);
});

router.post('/signup', function(req, res) {
  UserModel.findOne({'email': req.body.email}).then(user => {
    if (user) return res.status(401).send(`User with ${req.body.email} already exists`);

    let newUser = new UserModel();
    Object.keys(req.body).forEach(k => {
      if (k === 'password') {
        newUser.password = newUser.generateHash(req.body[k]);
      } else {
        newUser[k] = req.body[k];
      }
    });
    newUser.createdOn = new Date();

    return newUser.save()
  }).then(user => {
    res.send(user);
  }).catch(err => {
    res.sendStatus(500);
  });
});

router.put('/edit-user', isAuthenticated, (req, res) => {
  if (!req.user.isAdmin && req.body._id !== req.user._id.toString()) {
    res.sendStatus(401);
  } else {
    UserModel.findOne({'_id': req.body._id}).then(user => {
      if (req.body.passwordChange && !user.verifyPassword(req.body.passwordChange.oldPassword)) {
        return res.sendStatus(401);
      }
      let updateFields = ['firstName', 'lastName', 'email', 'phone'];
      updateFields.forEach(field => {
        user[field] = req.body[field];
      });

      if (req.body.passwordChange) {
        user.password = user.generateHash(req.body.passwordChange.password);
      }
      user.save().then((user) => {
        res.send(user);
      }).catch(err => {
        res.sendStatus(500);
      });
    });
  }
});

router.post('/contact-form', (req, res) => {
  UserModel.find({'isAdmin': true}).then(users => {
    let defaultEmailTemplate = fs.readFileSync(process.cwd() + '/server/templates/default-template.html', 'utf8');
    let mailTemplate = Handlebars._compileTemplate(defaultEmailTemplate);
    let mailContent = mailTemplate({content: `Получено новое сообщение от пользователя. <br> Имя пользователя: ${req.body.name}. <br> Email: ${req.body.email}. <br> Сообщение: ${req.body.message}.`});

    const mailOptions = {
      from: 'Infamiss Shop <prozrachniy@gmail.com>',
      to: users.map(u => u.email),
      subject: 'Сообщение от пользователя',
      html: mailContent
    };

    transporter.sendMail(mailOptions, function (err, info) {
       if (err) {
         res.sendStatus(500);
       } else {
         res.sendStatus(200);
       }
    });
  }).catch((err) => {
    res.sendStatus(500);
  })
});

module.exports = router

