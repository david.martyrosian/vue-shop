const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy;

const User = require('../mongo/model/user');

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  User.findById(id, function(err, user) {
    done(err, user);
  });
});

module.exports = function(passport) {
  passport.use(new LocalStrategy(
    {
      usernameField: 'email',
      passwordField: 'password'
    },
    function(username, password, cb) {
      User.findOne({ email: username }, function (err, user) {
        if (err) { return cb(err); }
        if (!user) {
          return cb('Incorrect username.', false, { message: 'Incorrect username.' });
        }
        if (user.password && !user.verifyPassword(password)) {
          return cb('Incorrect password.', false, { message: 'Incorrect password.' });
        }
        return cb(null, user);
      });
    }));
};