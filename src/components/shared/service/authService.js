import axios from "axios";
axios.defaults.withCredentials = true;

/* Encryption User based on base64 */
export const encryptUser = (user) => {
    const objStr = JSON.stringify(user);
    const encryptedValue = new Buffer(objStr).toString("base64");
    return encryptedValue;
}

/* Decrypting User based on base64*/
export const decryptUser = () => {
    const data = localStorage.getItem("_auth");
    if (!data) {
        return null;
    }
    const strObj = new Buffer(data || "", "base64").toString("utf8");
    const value = JSON.parse(strObj);
    return value;
}

/* Verify that person is loggedIn */
export const isLoggedIn = () => {
    return axios.get(`${process.env.VUE_APP_BASE_URL}/is-authenticated`).then(resp => {
        return Promise.resolve(true);
    }).catch(err => {
        return Promise.resolve(false);
    });
}

export const isAdmin = () => {
    return axios.get(`${process.env.VUE_APP_BASE_URL}/is-admin`).then(resp => {
        return Promise.resolve(true);
    }).catch(err => {
        return Promise.resolve(false);
    });
}

/* Returning the logged User */
export const getLoggedInUser = () => {
    const data = decryptUser();
    return data;
}