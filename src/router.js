import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import {
    isLoggedIn, isAdmin
} from './components/shared/service/authService'

Vue.use(Router)

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    scrollBehavior() {
        return { x: 0, y: 0 };
    },
    routes: [{
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/about',
        name: 'about',
        component: () => import('./views/About.vue')
    },
    {
        path: '/products',
        name: 'products',
        component: () => import('./views/Products.vue')
    },
    {
        path: '/profile',
        name: 'profile',
        component: () => import('./views/Profile.vue'),
        beforeEnter: (to, from, next) => {
            return isLoggedIn().then(loggedIn => {
                if (loggedIn) {
                    next()
                } else {
                    next({
                        name: 'login',
                        query: {
                            from: to.name
                        }
                    })
                }
            });
        }
    },
    {
        path: '/admin',
        name: 'admin',
        component: () => import('./views/Admin.vue'),
        beforeEnter: (to, from, next) => {
            return isAdmin().then(loggedIn => {
                if (loggedIn) {
                    next()
                } else {
                    next({
                        name: 'login',
                        query: {
                            from: to.name
                        }
                    })
                }
            });
        }
    },
    {
        path: '/products/:id',
        name: 'productDetails',
        component: () => import('./components/products/ProductDetail.vue')
    },
    {
        path: '/cart',
        name: 'cart',
        component: () => import('./components/products/cart/CartProducts.vue'),
        beforeEnter: (to, from, next) => {
            return isLoggedIn().then(loggedIn => {
                if (loggedIn) {
                    next()
                } else {
                    next({
                        name: 'login',
                        query: {
                            from: to.name
                        }
                    })
                }
            });
        }
    },
    {
        path: '/checkout',
        name: 'checkout',
        component: () => import('./components/products/cart/Checkout.vue'),
        beforeEnter: (to, from, next) => {
            if (isLoggedIn()) {
                next()
            } else {
                next({
                    name: 'login',
                    query: {
                        from: to.name
                    }
                })
            }
        }
    },
    {
        path: '/login',
        name: 'login',
        component: () => import('./views/login.vue')
    },
    {
        path: '/forgot-password/:token',
        name: 'forgot-password',
        component: () => import('./components/auth/forgot-password.vue')
    }
    ]
})
