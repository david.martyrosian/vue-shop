module.exports = {
  css: {
    loaderOptions: {
      sass: {
        data: `
          @import "@/style/_vars.scss";
        `
      }
    }
  }
};