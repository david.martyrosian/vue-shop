// Get dependencies
const express = require('express')
const https = require('https');
const http = require('http');
const fs = require('fs');

const bodyParser = require('body-parser')
const history = require('connect-history-api-fallback')
const path = require('path')
// Get our API routes
const mongodb = require('./server/mongo/config')
const passport = require('passport');
require('./server/passport/passport')(passport);

const api = require('./server/routes/api')
const productApi = require('./server/routes/productApi')
const authApi = require('./server/routes/authApi')
const cors = require('cors')
require('dotenv').config()
const app = express()
let options;
if (process.env.ENV === 'PRODUCTION') {
   options = {
    key: fs.readFileSync('/etc/letsencrypt/live/martyrosiandavid.com/privkey.pem'),
    cert: fs.readFileSync('/etc/letsencrypt/live/martyrosiandavid.com/fullchain.pem')
  };
}

// Parsers for POST data
app.use('/src', express.static(__dirname + '/src'));
app.use('/public', express.static(__dirname + '/public'));

app.use(bodyParser.json())
app.use(cors({credentials: true, origin: ['http://167.99.42.184', 'https://www.martyrosiandavid.com', 'https://martyrosiandavid.com', 'http://www.martyrosiandavid.com', 'http://martyrosiandavid.com', 'http://localhost:3000'] }))

app.use(bodyParser.urlencoded({
    extended: false
}))
app.use(require('cookie-parser')('keyboard cat'));
app.use(require('express-session')({ secret: 'keyboard cat', cookie: { secure: false, maxAge : 36000000 }, resave: false, saveUninitialized: false }));

app.use(passport.initialize());
app.use(passport.session());

var distDir = __dirname + "/dist";
app.use(express.static(distDir));


app.use(history({
    index: '/index.html'
}))


app.use('/api', [api, productApi, authApi])

/// catch 404 and forward to error handler
app.use(function(req, res, next) {
  console.log('error');
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.use(function(err, req, res, next) {
  console.log('error1');
  res.status(err.status || 500);
  res.json({'errors': {
    message: err.message,
    error: {}
  }});
});

if (process.env.ENV === 'PRODUCTION') {
  https.createServer(options, app).listen(8080, function() {
    console.log('HTTPS App now running on port', 8080);
  });

  http.createServer(app).listen(8081, function () {
      console.log('App now running on port', 8081)
  });
} else {
  http.createServer(app).listen(8080, function () {
      console.log('App now running on port', 8080)
  });
}
